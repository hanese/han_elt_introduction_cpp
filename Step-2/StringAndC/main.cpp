// C++ string objects can be used as a C char array for C functions.

#include <iostream>
#include <string>

// C function
extern "C" {
int countChar(const char *str, char c);
}

int main()
{
   std::string str1 = "Hello C++ World";
   char c = '+';

   std::cout << "Counting " << c << " in \"" << str1
             << "\" = " << countChar(str1.c_str(), c) << std::endl;

   return 0;
}

extern "C" {

int countChar(const char *str, char c)
{
   int count = 0;
   int i = 0;

   while (str[i] != '\0') {
      if (str[i] == c) {
         count++;
      }
      i++;
   }
   return count;
}
}
