// C++ class std::array<T, size> is a templated container class that
// encapsulates a fixed size array (no dynamic memory allocation).

#include <algorithm>
#include <array>
#include <iostream>

// Modern C++ typedef
using data_int_4_t = std::array<int, 4>;
using data_int_6_t = std::array<int, 6>;
using data_double_8_t = std::array<double, 8>;

template <class T, size_t SIZE>
T findMax(const std::array<T, SIZE> &data);

int main()
{
   data_int_4_t a1{-1, 2, -3, 4};
   // Function template instantiation.
   auto max_int_4 = findMax(a1);
   std::cout << "Result max_int_4 = " << max_int_4 << "\n";

   data_int_6_t a2{-4, -9, 5, 10, 25, 15};
   // Function template instantiation..
   auto max_int_6 = findMax(a2);
   std::cout << "Result max_int_6 = " << max_int_6 << "\n";

   data_double_8_t a3{-4.4, -9.9, 5.5, 10.10, 25.25, 15.15, -40.40, 25.25};
   // Function template instantiation.
   auto max_double_8 = findMax(a3);
   std::cout << "Result max_double_8 = " << max_double_8 << "\n";

   return 0;
}

// Function template, template definition.
// Modern C++ implementation, using range-based for loop, auto and std::max().
template <class T, size_t SIZE>
T findMax(const std::array<T, SIZE> &data)
{
   // Obtain the minimum value for type T
   T max = std::numeric_limits<T>::min();

   for (const T &e : data) {
      max = std::max(max, e);
   }

   return max;
}
