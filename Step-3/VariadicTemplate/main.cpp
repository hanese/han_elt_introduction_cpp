// C++11 variadic template

#include <iostream>

template <typename T>
T add(T arg)
{
   return arg;
}

// Recursive implementation: add() calls add()
template <typename T, typename... Args>
T add(T first, Args... args)
{
   return first + add(args...);
}

int main()
{
   std::cout << "add(1) = " << add(1) << "\n";
   std::cout << "add(1, 2) = " << add(1, 2) << "\n";
   std::cout << "add(1, 2, 3) = " << add(1, 2, 3) << "\n";
   std::cout << "add(1, 2, 3, 4) = " << add(1, 2, 3, 4) << "\n";
   std::cout << "add(1, 2, 3, 4, 5, 6, 7, 8, 9, 10) = "
             << add(1, 2, 3, 4, 5, 6, 7, 8, 9, 10) << "\n\n";

   std::cout << "add(1, 2.2, 3.3, 4) = " << add(1, 2.2, 3.3, 4) << "\n";
   std::cout << "add(1.1, 2.2, 3.3, 4) = " << add(1.1, 2.2, 3.3, 4) << "\n";

   return 0;
}
