// C++ class std::array<T, size> is a templated container class that
// encapsulates a fixed size array (no dynamic memory allocation).

#include <algorithm>
#include <array>
#include <iostream>
#include <limits>

int maxArray(const std::array<int, 4> &data);
int maxArray2(const std::array<int, 4> &data);

int main()
{
   // array is a container templated class
   std::array<int, 4> a1{
      {-1, 2, -3, 4}};         // list initialisation, double braces required
   std::array<int, 4> ar2{a1}; // copy ctor

   std::cout << "Max value in array a1 = " << maxArray(a1) << std::endl;
   std::cout << "Max value in array a1 = " << maxArray2(a1) << std::endl;

   std::cout << "All values in ar2 = ";
   // Range-based for loop, no index anymore!
   // element contains the value (copy) of an ar2 element.
   for (auto element : ar2) {
      std::cout << element << " ";
   }
   std::cout << std::endl;

   std::cout << "Sorted ar2 = ";
   std::sort(begin(ar2), end(ar2));
   for (auto element : ar2) {
      std::cout << element << " ";
   }
   std::cout << std::endl;

   return 0;
}

int maxArray(const std::array<int, 4> &data)
{
   auto max = data[0]; // automatic type deduction

   for (auto i = 1; i < data.size(); i++) {
      if (data[i] > max) {
         max = data[i];
      }
   }

   return max;
}

// Modern C++ implementation, less code!
int maxArray2(const std::array<int, 4> &data)
{
   auto max = std::numeric_limits<int>::min();

   // Range-based for loop, d is a copy of array element
   for (auto d : data) {
      max = std::max(max, d);
   }

   return max;
}
