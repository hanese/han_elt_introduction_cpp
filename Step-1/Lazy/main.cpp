#include <iostream>

bool f1();
bool f2();

int main()
{
   // Lazy evaluation logical && expression
   std::cout << "Execute f1() && f2(): ";
   if (f1() && f2()) {
      std::cout << "f1() && f2() == true\n\n";
   } else {
      std::cout << "f1() && f2() == false\n\n";
   }

   // Lazy evaluation logical || expression
   std::cout << "Execute f2() || f1(): ";
   if (f2() || f1()) {
      std::cout << "f2() || f1() == true\n\n";
   } else {
      std::cout << "f2() || f1() == false\n\n";
   }

   return 0;
}

bool f1()
{
   std::cout << "f1() executed => false\n";
   return false;
}

bool f2()
{
   std::cout << "f2() executed => true\n";
   return true;
}
