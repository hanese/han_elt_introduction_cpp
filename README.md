# Introduction C++

## Introduction C++ programming for C programmers

Necessary prior knowledge:

- **Introduction C** <https://github.com/josokw/IntroductionC>
- **Advanced C** <https://gitlab.com/hanese/han_elt_advancedc>

## Step-1

- Lazy: lazy evaluation of logical expressions.
- Namespaces: **::** scope resolution operator, std namespace.
- IOstreams: using namespace std, IO, cout, << operator, cin, >> operator.
- Formatting: formatted IO.
- FunctionOverloading: functions may have the same name, but a different signature (function name and parameter list), the return type is not part of the signature!

## Step-2

- CallByReference: in C only call by reference with pointers, in C++ call by reference with references
- String: std::string
- StringAndC: mixing C and C++ strings

## Step-3

- Array class: standard array templated class <....>, container class, range based for loop, auto type deduction.
- ArrayInParameterList: templated function to determine the maximum value in a std::array.
- ForwardList class: standard SLL.
- Rectangle: user defined class implementation in C++11, constructor, constructor initialization list, constructor overloading, class data members, getters, setters, const correct.
- Point class: user defined class modular implementation in C++03, header and implementation file.
- ObjectSelfie class: shows memory adresses of class member functions and data, this pointer, static class member functions and data.
- Variadic template function in C++11.

## Step-4-GUI

- BasiclayoutsLEDs-gui: shows the use of layouters and widgets, Dialog class is the main window class, contains an additional Led class, Qt designer is not used.
- CVM-gui: QtCreator GUI for Cola Vending Machine, contains StateMachine class.
